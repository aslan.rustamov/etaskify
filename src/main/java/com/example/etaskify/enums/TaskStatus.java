package com.example.etaskify.enums;

public enum TaskStatus {

    ASSIGNED, IN_PROCESS, FINISHED, CLOSED
}
