package com.example.etaskify.mapper;

import com.example.etaskify.dto.request.CreateUserRequestDto;
import com.example.etaskify.dto.response.UserResponseDto;
import com.example.etaskify.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface UserMapper {

    @Mapping(source = "username", target = "userName")
    @Mapping(source = "firstname", target = "firstName")
    @Mapping(source = "lastname", target = "lastName")
    User toEntity(CreateUserRequestDto createUserRequestDto);

    UserResponseDto toDto(User user);
}
