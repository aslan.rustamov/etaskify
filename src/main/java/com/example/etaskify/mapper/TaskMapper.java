package com.example.etaskify.mapper;

import com.example.etaskify.dto.request.CreateTaskRequestDto;
import com.example.etaskify.dto.response.TaskResponseDto;
import com.example.etaskify.entity.Task;
import org.mapstruct.Mapper;

@Mapper
public interface TaskMapper {

    Task mapToEntity(CreateTaskRequestDto createTaskRequestDto);

    TaskResponseDto mapToDto(Task task);
}
