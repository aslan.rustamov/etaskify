package com.example.etaskify.dto.response;

import lombok.Data;

@Data
public class UserResponseDto {
    private long id;
    private String firstName;
    private String lastName;
    private String userName;
}
