package com.example.etaskify.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class RegistrationDto {

    @NotBlank(message = "Username can't be empty!")
    private String username;

    @NotBlank(message = "Password can't be empty!")
    @Size(min = 6, message = "Password length must be at least 6 characters")
    private String password;

    @NotBlank(message = "Email can't be empty!")
    private String email;

    @NotBlank(message = "Organization can't be empty!")
    private String organization;

    @NotBlank(message = "Phone number can't be empty!")
    private String phoneNumber;

    @NotBlank(message = "Address can't be empty!")
    private String address;

}
