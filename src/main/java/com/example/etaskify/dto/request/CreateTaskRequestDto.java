package com.example.etaskify.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class CreateTaskRequestDto {

    @NotBlank(message = "Title can't be blank")
    private String title;

    private String description;

    @NotNull(message = "Deadline can't be blank")
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm", iso= DateTimeFormat.ISO.DATE_TIME)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime deadLine;

    private List<Long> userIds;
}
