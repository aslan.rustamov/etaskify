package com.example.etaskify.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EmailDto {

    private String to;
    private String body;
    private String subject;
}
