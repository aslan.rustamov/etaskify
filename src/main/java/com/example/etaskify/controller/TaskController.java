package com.example.etaskify.controller;

import com.example.etaskify.dto.request.CreateTaskRequestDto;
import com.example.etaskify.dto.response.TaskResponseDto;
import com.example.etaskify.service.TaskService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class TaskController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/api/tasks")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> create(@Valid @RequestBody CreateTaskRequestDto createTaskRequestDto) {
        return ResponseEntity.ok(taskService.createTaskAndAssignToUsers(createTaskRequestDto));
    }

    @GetMapping("/api/tasks")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<TaskResponseDto>> getOrganizationTasks() {
        return ResponseEntity.ok(taskService.getOrganizationTasks());
    }

    @GetMapping("/api/user/tasks")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<List<TaskResponseDto>> getUserTasks() {
        return ResponseEntity.ok(taskService.getUserTasks());
    }
}
