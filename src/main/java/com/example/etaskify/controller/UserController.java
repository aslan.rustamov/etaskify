package com.example.etaskify.controller;

import com.example.etaskify.dto.request.CreateUserRequestDto;
import com.example.etaskify.dto.response.UserResponseDto;
import com.example.etaskify.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> create(@Valid @RequestBody CreateUserRequestDto createUserRequestDto) {
        userService.create(createUserRequestDto);
        return ResponseEntity.ok("User created successfully!");
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<UserResponseDto>> getUsersByOrganization() {
        return ResponseEntity.ok(userService.getUsersByOrganization());
    }
}
