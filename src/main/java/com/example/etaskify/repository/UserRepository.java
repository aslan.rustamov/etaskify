package com.example.etaskify.repository;

import com.example.etaskify.entity.Organization;
import com.example.etaskify.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUserName(String username);

    List<User> findAllByOrganization(Organization organization);


}
