package com.example.etaskify.service;

import com.example.etaskify.dto.request.RegistrationDto;
import com.example.etaskify.enums.Role;
import com.example.etaskify.entity.Organization;
import com.example.etaskify.entity.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserService userService;
    private final OrganizationService organizationService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public void signIn(RegistrationDto registrationDto) {

        Organization newOrganization = Organization.builder()
                .organizationName(registrationDto.getOrganization())
                .phoneNumber(registrationDto.getPhoneNumber())
                .address(registrationDto.getAddress())
                .build();
        Organization organization = organizationService.create(newOrganization);

        User user = User.builder()
                .userName(registrationDto.getUsername())
                .password(bCryptPasswordEncoder.encode(registrationDto.getPassword()))
                .email(registrationDto.getEmail())
                .role(Role.ADMIN)
                .organization(organization)
                .build();
        userService.save(user);

    }
}
