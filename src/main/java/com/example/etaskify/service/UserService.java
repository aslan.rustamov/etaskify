package com.example.etaskify.service;

import com.example.etaskify.dto.request.CreateUserRequestDto;
import com.example.etaskify.dto.response.UserResponseDto;
import com.example.etaskify.enums.Role;
import com.example.etaskify.exception.UserNotFoundException;
import com.example.etaskify.mapper.UserMapper;
import com.example.etaskify.entity.User;
import com.example.etaskify.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final OrganizationService organizationService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public List<User> getUsersByIds(List<Long> userIds) {
        return userRepository.findAllById(userIds);
    }

    public User getUserByUsername(String username) {
        return userRepository.findByUserName(username).orElseThrow(() -> new UserNotFoundException("User not found!"));
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public User create(CreateUserRequestDto createUserRequestDto) {
        User user = userMapper.toEntity(createUserRequestDto);
        user.setRole(Role.USER);
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setOrganization(organizationService.getOrganization());
        return userRepository.save(user);
    }

    public List<UserResponseDto> getUsersByOrganization() {
        return userRepository.findAllByOrganization(organizationService.getOrganization())
                .stream()
                .map(user -> userMapper.toDto(user))
                .collect(Collectors.toList());
    }

}
