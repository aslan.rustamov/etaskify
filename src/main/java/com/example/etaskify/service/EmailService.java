package com.example.etaskify.service;

import com.example.etaskify.dto.EmailDto;

public interface EmailService {

    void sendEmail(EmailDto emailDto);

}
