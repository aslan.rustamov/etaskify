package com.example.etaskify.service;

import com.example.etaskify.dto.EmailDto;
import com.example.etaskify.dto.request.CreateTaskRequestDto;
import com.example.etaskify.dto.response.TaskResponseDto;
import com.example.etaskify.enums.TaskStatus;
import com.example.etaskify.mapper.TaskMapper;
import com.example.etaskify.entity.Task;
import com.example.etaskify.entity.User;
import com.example.etaskify.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;
    private final UserService userService;
    private final UserPrincipalService userPrincipalService;
    private final OrganizationService organizationService;
    private final EmailService emailService;

    public TaskResponseDto createTaskAndAssignToUsers(CreateTaskRequestDto createTaskRequestDto) {
        List<User> users = userService.getUsersByIds(createTaskRequestDto.getUserIds());
        Task task = taskMapper.mapToEntity(createTaskRequestDto);
        task.setOrganization(organizationService.getOrganization());
        task.setStatus(TaskStatus.ASSIGNED);
        task.setUsers(users);
        Task savedTask = taskRepository.save(task);
        sendToUsersAboutAssignedTask(users, task);
        return taskMapper.mapToDto(savedTask);
    }

    public List<TaskResponseDto> getOrganizationTasks() {
        return taskRepository.findAllByOrganization(organizationService.getOrganization())
                .stream()
                .map(taskMapper::mapToDto)
                .collect(Collectors.toList());
    }

    public List<TaskResponseDto> getUserTasks() {
        User user = userPrincipalService.getCurrentUser();
        return user.getTasks()
                .stream()
                .map(taskMapper::mapToDto)
                .collect(Collectors.toList());
    }

    private void sendToUsersAboutAssignedTask(List<User> users, Task task) {
        users.forEach(user -> {
            EmailDto emailDto = EmailDto.builder()
                    .to(user.getEmail())
                    .body(String.format("Task id: %d, title: %s, description: %s",
                            task.getId(), task.getTitle(), task.getDescription()))
                    .subject("There is a task for you ;)")
                    .build();
            emailService.sendEmail(emailDto);
        });
    }
}
