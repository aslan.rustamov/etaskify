package com.example.etaskify.service;

import com.example.etaskify.entity.Organization;
import com.example.etaskify.entity.User;
import com.example.etaskify.repository.OrganizationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrganizationService {

    private final OrganizationRepository organizationRepository;
    private final UserPrincipalService userPrincipalService;

    public Organization create(Organization organization) {
        return organizationRepository.save(organization);
    }

    public Organization getOrganization() {
        User user = userPrincipalService.getCurrentUser();
        return user.getOrganization();
    }

}
