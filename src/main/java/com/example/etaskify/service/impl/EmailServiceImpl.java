package com.example.etaskify.service.impl;

import com.example.etaskify.dto.EmailDto;
import com.example.etaskify.service.EmailService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Email;

@Service
public class EmailServiceImpl implements EmailService {

    private final MailSender mailSender;

    @Value("${spring.mail.username}")
    private String email;

    public EmailServiceImpl(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void sendEmail(EmailDto emailDto) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(email);
        mailMessage.setTo(emailDto.getTo());
        mailMessage.setText(emailDto.getBody());
        mailMessage.setText(emailDto.getSubject());
        mailSender.send(mailMessage);
    }
}
