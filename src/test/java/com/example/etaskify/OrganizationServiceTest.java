package com.example.etaskify;

import com.example.etaskify.entity.Organization;
import com.example.etaskify.entity.User;
import com.example.etaskify.service.OrganizationService;
import com.example.etaskify.service.UserPrincipalService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class OrganizationServiceTest {


    @Autowired
    private OrganizationService organizationService;

    @MockBean
    private UserPrincipalService userPrincipalService;

    private List<User> users = new ArrayList<>();
    private List<Organization> organizations = new ArrayList<>();

    @BeforeEach
    void setUp() {
        Organization organization1 = new Organization();
        organization1.setId(1L);
        organization1.setOrganizationName("Organizaltion 1");
        organization1.setAddress("Lorem ipsum");
        organization1.setPhoneNumber("+994555555555");

        Organization organization2 = new Organization();
        organization2.setId(2L);
        organization2.setOrganizationName("Organization 2");
        organization2.setAddress("Lorem ipsum 2");
        organization2.setPhoneNumber("+994509999999");

        organizations.add(organization1);
        organizations.add(organization2);

        User user1 = new User();
        user1.setId(1L);
        user1.setFirstName("Lorem 1");
        user1.setLastName("Ipsum 1");
        user1.setOrganization(organization1);

        User user2 = new User();
        user2.setId(2L);
        user2.setFirstName("Lorem 2");
        user2.setLastName("Ipsum 2");
        user2.setOrganization(organization2);

        users.add(user1);
        users.add(user2);
    }

    @Test
    void getMyOrganization() {
        Mockito.doReturn(users.get(1)).when(userPrincipalService).getCurrentUser();
        Organization organization = organizationService.getOrganization();

        Assert.assertNotNull(organization);
        Assert.assertEquals(users.get(1).getOrganization(), organization);
    }

}